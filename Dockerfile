#-------------------------------------------------------------------------------
FROM node:8.11.3-alpine AS base

WORKDIR /opt/fiware-pep-proxy

#-------------------------------------------------------------------------------
FROM base AS dependencies

RUN apk update && \
  apk add python build-base

# Install dependencies & devDependencies
COPY package-lock.json .
COPY package.json .
RUN npm install --production

#-------------------------------------------------------------------------------
FROM base AS release

COPY --from=dependencies /opt/fiware-pep-proxy/package-lock.json .
COPY --from=dependencies /opt/fiware-pep-proxy/package.json .
COPY --from=dependencies /opt/fiware-pep-proxy/node_modules node_modules

# Copy source folders
COPY controllers controllers
COPY db db
COPY lib lib
COPY policies policies
COPY server.js .
COPY log_config.json log_config.json

# Run PEP Proxy
CMD ["node", "server.js"]
