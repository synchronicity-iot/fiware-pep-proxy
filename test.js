const test = require('ava')
const request = require('supertest')
const nock = require('nock')

test('Ensure IDM is called for non-public paths', async t => {
  const fakeIdm = nock('http://testidm')
    .post('/v3/auth/tokens')
    .reply(
      200,
      {
        token: {
          expires_at: '2099-01-01',
        },
      },
      {
        'x-subject-token': 'fake-subject-token',
      }
    )
    .get('/v3/access-tokens/fakeToken')
    .reply(200, {
      app_id: 'testAppId',
      id: '123',
      displayName: 'Display Name',
      roles: [],
      organizations: [],
    })

  const fakeApp = nock('http://testapp')
    .get('/foo')
    .reply(200, '{"foo": "bar"}', { 'x-header': 'placeholder' })

  const app = require('./server')

  const res = await request(app)
    .get('/foo')
    .set('authorization', 'Bearer ZmFrZVRva2Vu')
    .unset('User-Agent')
  
  t.is(res.status, 200)
  t.is(res.text, '{"foo": "bar"}')

  // Ensure IDM was called
  t.true(fakeIdm.isDone())
  t.true(fakeApp.isDone())
})
